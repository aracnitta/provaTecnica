//
//  DetalheMusicaViewController.swift
//  provaTecnica2
//
//  Created by COTEMIG on 25/08/22.
//

import UIKit

class DetalheMusicaViewController: UIViewController {
    
    var nomeImagem: String = ""
    var nomeMusica: String = ""
    var nomeAlbum: String = ""
    var nomeCantor: String = ""
    
    @IBOutlet weak var titulo: UILabel!
    @IBOutlet weak var album: UILabel!
    @IBOutlet weak var cantor: UILabel!
    @IBOutlet weak var imagem: UIImageView!
    override func viewDidLoad() {
        super.viewDidLoad()

        self.imagem.image = UIImage(named: self.nomeImagem)
        self.titulo.text = self.nomeMusica
        self.album.text = self.nomeAlbum
        self.cantor.text = self.nomeCantor
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
