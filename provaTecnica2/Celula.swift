//
//  Celula.swift
//  provaTecnica2
//
//  Created by COTEMIG on 25/08/22.
//

import UIKit

class Celula: UITableViewCell {

    @IBOutlet weak var titulo: UILabel!
    @IBOutlet weak var cantor: UILabel!
    @IBOutlet weak var album: UILabel!
    @IBOutlet weak var imagem: UIImageView!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
